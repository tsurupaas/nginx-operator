module github.com/nettoclaudio/nginx-operator

go 1.12

require (
	cloud.google.com/go v0.35.1 // indirect
	github.com/go-logr/logr v0.1.0 // indirect
	github.com/go-logr/zapr v0.1.1 // indirect
	github.com/gogo/protobuf v1.2.0 // indirect
	github.com/golang/groupcache v0.0.0-20181024230925-c65c006176ff // indirect
	github.com/googleapis/gnostic v0.2.0 // indirect
	github.com/howeyc/fsnotify v0.9.0 // indirect
	github.com/imdario/mergo v0.3.7 // indirect
	github.com/mattbaird/jsonpatch v0.0.0-20171005235357-81af80346b1a // indirect
	github.com/operator-framework/operator-sdk v0.0.0-20190123184538-3c97587d829f
	github.com/prometheus/client_model v0.0.0-20190115171406-56726106282f // indirect
	github.com/prometheus/common v0.1.0 // indirect
	github.com/prometheus/procfs v0.0.0-20190117184657-bf6a532e95b1 // indirect
	github.com/stretchr/testify v1.3.0
	github.com/tsuru/config v0.0.0-20180418191556-87403ee7da02
	github.com/tsuru/nginx-operator v0.0.0-20190723231449-5e766037eedd
	go.uber.org/atomic v1.3.2 // indirect
	go.uber.org/multierr v1.1.0 // indirect
	go.uber.org/zap v1.9.1 // indirect
	k8s.io/api v0.0.0-20190726022912-69e1bce1dad5
	k8s.io/apiextensions-apiserver v0.0.0-20190726024412-102230e288fd // indirect
	k8s.io/apimachinery v0.0.0-20190726022757-641a75999153
	k8s.io/client-go v0.0.0-20190726023111-a9c895e7f2ac
	sigs.k8s.io/controller-runtime v0.1.8
	sigs.k8s.io/testing_frameworks v0.1.1 // indirect
)
